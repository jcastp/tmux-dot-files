# tmux-dot-files
My own dot files for tmux

## requisites
- [tmux plugin manager](https://github.com/tmux-plugins/tpm)
- powerline (install in the OS), needed for the status line
- xsel (install in the OS), needed for tmux-yank

## plugins
- tmux-sensible
- tmux-yank
- tmux-cpu
- tmux-open
- tmux-resurrect: used to save and load sessions
